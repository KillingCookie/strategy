#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "SDL.h"

#include "../igame.h"

class IGame;

class Keyboard {
private:
	bool keys[256] = { 0 }, keysPressed[256] = { 0 }, keysReleased[256] = { 1 }, keysTyped[256] = { 0 };
	IGame*& game;
	SDL_Event* e;

public:
	bool& up, & down, & left, & right;

	Keyboard(IGame* game);

	void tick();

	void typed();
	void pressed();
	void released();

};

#endif KEYBOARD_H