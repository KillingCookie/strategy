#ifndef ENTITY_COMPONENT_MANAGER_H
#define ENTITY_COMPONENT_MANAGER_H

#include <unordered_map>
#include <vector>

#include "entity/entity_manager.h"
#include "component/component.h"

namespace ecs {

	using entity = uint16_t;

	class IEntityComponentArray {
		virtual void EntityDestroyed(entity*& ent) = 0;
	};

	template<class T_Comp>
	class EntityComponentArray : public IEntityComponentArray{
	public:
		void AttachComponent(entity*& ent, std::string name, T_Comp*& comp) {
			this->components.insert(std::make_pair(ent, std::make_pair(name, comp)));
		}
		void RemoveComponent(entity*& ent) {//string for entity type
			this->components.erase(ent);
		}
		void EntityDestroyed(entity*& ent) {
			this->components.erase(ent);
		}
		T_Comp*& GetData(entity*& ent, std::string type_name) { 
			std::pair<std::unordered_multimap <entity*, std::pair<std::string, T_Comp*>>::iterator,
						std::unordered_multimap <entity*, std::pair<std::string, T_Comp*>>::iterator> search = this->components.equal_range(ent);
			auto itr = search.first;
			for (; itr->second.first != type_name; ++itr);
			return itr->second.second;
		}
	private:
		//std::vector<T_Comp> all_components;
		std::unordered_multimap <entity*, std::pair<std::string, T_Comp*>> components;
	};


}

#endif // !ENTITY_COMPONENTS_H
