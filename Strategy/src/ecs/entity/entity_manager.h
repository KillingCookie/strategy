#ifndef ENTITY_MANAGER_H
#define ENTITY_MANAGER_H

#include <cstdint>
#include <bitset>
#include <deque>
//#include <string>

namespace ecs {
	using entity = uint16_t;
	using entityId = uint16_t;
	using componentType = uint16_t;
	const componentType MAX_COMPONENT_TYPES = 256;
}

#include "../component/component_manager.h"

namespace ecs {

	class EntityManager {
	public:
		~EntityManager() {
			delete instance;
		}

		entity*& CreateEntity();
		void DestroyEntity(entity*& ent);

		entityId GetFreeId();

		static EntityManager* GetEntityManager() {
			if (instance == nullptr) instance = new EntityManager();
			return instance;
		}

		static const entityId MAX_ENTITIES = 65535;
	private:
		EntityManager() {}
		static EntityManager* instance;
		std::bitset<MAX_ENTITIES> used_ids;
		std::vector<std::bitset<MAX_COMPONENT_TYPES>> entity_components;
		std::deque<entity*> all_entities;
	};

}

#endif // !ENTITYMANAGER_H
