#include "entity_manager.h"

using namespace ecs;

EntityManager* EntityManager::instance = nullptr;

entityId EntityManager::GetFreeId() {
	for (entityId i = 0; i < EntityManager::MAX_ENTITIES; i++) {
		if (!this->used_ids[i]) {
			this->used_ids[i].flip();
			return i;
		}
	}
}

entity*& EntityManager::CreateEntity() {
	this->all_entities.push_back(new entityId(this->GetFreeId()));

	return this->all_entities.back();
}

void EntityManager::DestroyEntity(entity*& ent){
	std::deque<entity*>::iterator itr;
	for (itr = this->all_entities.begin(); itr != this->all_entities.end(); itr++){
		if (*itr == ent) {
			this->used_ids[itr - all_entities.begin()].flip();//shouldnt I remove it from all_entities?
			delete (*itr);
			(*itr) = nullptr;
		}
	}
}
