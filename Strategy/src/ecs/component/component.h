#ifndef COMPONENT_H
#define COMPONENT_H

#include <cstdint>
#include <string>

#include "../../physics/vector2d.h"

namespace ecs {

	struct Component {
		Component(std::string str): type_name(str){}
		const std::string type_name;
	};

	namespace defaultComponents {

		struct EntityName : Component {
			EntityName(std::string name) :Component("cell_type_component"), name(name) {}
			std::string name;
		};

		struct Position2D : Component {
			Position2D(Vector2D vec) : Component("position2D_component"), position(vec) {}
			Vector2D position;
		};

		struct CellType : Component {
			CellType(std::string type) :Component("cell_type_component") {
				if (type == "water") {
					this->type = (int)cellType::WATER;
				}
				else if (type == "plains") {
					this->type = (int)cellType::PLAINS;
				}
				else if (type == "forest") {
					this->type = (int)cellType::FOREST;
				}
				else if (type == "hills") {
					this->type = (int)cellType::HILLS;
				}
				else {
					this->type = 0;
				}
			}
			enum class cellType { WATER, PLAINS, FOREST, HILLS};
			uint8_t type;
		};

	}

}

#endif
