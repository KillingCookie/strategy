#ifndef COMPONENT_MANAGER_H
#define COMPONENT_MANAGER_H

#include <bitset>
#include <deque>
#include <unordered_map>
#include <string>
#include <typeinfo>

#include "../entity/entity_manager.h"
#include "component.h"
#include "../entity_component_array.h"

/**
*	Some initializer for all basic components???
*/

namespace ecs {

	using entity = uint16_t;

	class ComponentManager {
	public:
		~ComponentManager() {
			delete instance;
		}
			
		/* registers a component type into entity-component array */
		template<class T>
		void RegisterComponent() {//should be added by a typename field inside the component structure
			//if (!isRegistered<T>()) {
				this->existing_component_types.insert({ typeid(T).name(), GetFreeId() });
				this->component_arrays.insert({ typeid(T).name(), new EntityComponentArray<T>() });
			//}
			////???????/
		}

		/* adds component into entity-component array */
		template<class T>
		void AddComponent(entity*& ent, std::string name, T *comp){
			if (!isRegistered<T>()) RegisterComponent<T>();

			GetComponentArray<T>()->AttachComponent(ent, name, comp);
		}
		template<class T>
		void RemoveComponent(entity*& ent){}

		//template<

		Component* GetComponent(std::string) {

		}

		template<class T>
		T*& GetComponent(entity*& ent, std::string type_name){
			return GetComponentArray<T>()->GetData(ent, type_name);
		}

		template<class T>
		bool isRegistered() {
			if (this->existing_component_types.find(typeid(T).name()) == this->existing_component_types.end())
				return false;

			return true;
		}

		componentType GetFreeId();

		static ComponentManager* GetComponentManager() {
			if (instance == nullptr) instance = new ComponentManager();
			return instance;
		}
	private:
		ComponentManager() {}
		static ComponentManager* instance;
		std::bitset<MAX_COMPONENT_TYPES> used_types_id;//no need for bitset
		std::unordered_map<std::string, componentType> existing_component_types{};
		std::unordered_map<std::string, IEntityComponentArray*> component_arrays{};

		template<typename T>
		EntityComponentArray<T>* GetComponentArray(){
			if(isRegistered<T>())
				return (EntityComponentArray<T>*)(component_arrays[typeid(T).name()]);
		}
	};

}

#endif // !COMPONENT_MANAGER_H

