#ifndef VALUE_COMPONENT_H
#define VALUE_COMPONENT_H

#include "component.h"

namespace ecs {

	template<typename T>
	class ValueComponent : public Component {
	protected:
		ValueComponent(std::string name, T value) : Component(name), value_t(value) {}
		T GetValue() { return this->value_t; }
	private:
		T value_t;
	};

}

#endif
