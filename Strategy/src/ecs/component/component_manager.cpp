#include "component_manager.h"

using namespace ecs;

ComponentManager *ComponentManager::instance = nullptr;

componentType ComponentManager::GetFreeId() {
	for (componentType i = 0; i < MAX_COMPONENT_TYPES; i++) {
		if (!this->used_types_id[i]) {
			this->used_types_id[i].flip();
			return i;
		}
	}
}