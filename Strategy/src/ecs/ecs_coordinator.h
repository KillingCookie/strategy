#ifndef ECS_COORDINATOR_H
#define ECS_COORDINATOR_H

#include <string>

#include "component/component.h"
#include "entity/entity_manager.h"
#include "component/component_manager.h"

namespace ecs {

	class ECS_Coordinator {
	public:
		~ECS_Coordinator() {
			delete instance;
		}

		static ECS_Coordinator* GetECS_Coordinator() {
			if (instance == nullptr) instance = new ECS_Coordinator();
			return instance;
		}

		//name component as an argument
		entity*& CreateEntity(std::string ent_name) {
			entity* tmp_ent = this->ent_m->CreateEntity();
			defaultComponents::EntityName* name_component = new defaultComponents::EntityName(ent_name);
			this->comp_m->AddComponent<defaultComponents::EntityName>(tmp_ent, name_component->type_name, name_component);
			return tmp_ent;
		}

		template<typename T>
		void AddComponent(entity*& ent, T comp) {
			this->comp_m->AddComponent<T>(ent, comp);
		}

		template<typename T>
		T*& GetComponent(entity*& entity, std::string comp_type_name){
			return this->comp_m->GetComponent<T>(entity, comp_type_name);
		}

		template <typename T>
		void RegisterComponent() {
			this->comp_m->RegisterComponent<T>();
		}

		void RegisterDefaultComponents() {
			RegisterComponent<defaultComponents::EntityName>();
		}
	private:
		ECS_Coordinator() {
			this->ent_m = EntityManager::GetEntityManager();
			this->comp_m = ComponentManager::GetComponentManager();
			RegisterDefaultComponents();
		}

		static ECS_Coordinator* instance;
		EntityManager* ent_m;
		ComponentManager* comp_m;
	};

}

#endif // !ECS_COORDINATOR_H
