#ifndef STATE_MANAGER_H
#define STATE_MANAGER_H

#include<unordered_map>

#include "istate.h"

//singleton, use GetInstance()
class StateManager{
public:
	//enum class StateType : uint8_t { MENU_STATE, GAME_STATE, TOTAL_STATES };  //TOTAL_STATES is here only to know how many states can there be
	~StateManager() {
		delete current_state;
	}
	
	template<typename StateType>
	void SetState(IGame& game, SDL_Renderer*& r, Keyboard*& key, std::string folder_path) {
		if (!this->current_state) this->current_state = new StateType(game, r, key, folder_path);
	}

	void DeleteState() { 
		delete this->current_state; 
		this->current_state = nullptr;
	}

	//get current state
	IState* GetExistingState() { return this->current_state; }

	void TickState() { this->current_state->Tick(); }
	void RenderState() { this->current_state->Render(); }

	static StateManager* GetInstance() {
		if (instance == nullptr) instance = new StateManager();
		return instance;
	}

private:
	StateManager() {}
	IState* current_state = nullptr;
	static StateManager* instance;
	
};

#endif // STATE_MANAGER_H