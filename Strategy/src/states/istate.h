#ifndef ISTATE_H
#define ISTATE_H

#include <cstdint>
#include <string>

#include "SDL.h"

#include "../igame.h"
#include "../input/Keyboard.h"

class IState {
public:
	IState(IGame& game, SDL_Renderer* r, Keyboard* key, std::string& folder_path) : game(game), r(r), key(key), folder_path(folder_path){}

	virtual void Tick() = 0;
	virtual void Render() = 0;
private:
	IGame& game;

	SDL_Renderer*& r;
	Keyboard*& key;
	std::string& folder_path;
};

#endif ISTATE_H
