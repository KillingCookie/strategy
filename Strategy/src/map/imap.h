#ifndef IMAP_H
#define IMAP_H

#include <vector>
#include <list>
#include <cstdint>
#include <string>

#include "SDL.h"

#include "../utils/files/csv_file_manager.h"
#include "../ecs/ecs_coordinator.h"

class IMap {
public:
	/* takes string path of folder or *Map.csv file
	*  constructor creates an entity array of the map - map_array
	*  and creates components for each tile */
	IMap(std::string fpath) : fpath(fpath){}
	virtual void Tick() = 0;
	virtual void Render(SDL_Renderer*& r) = 0;
protected:
	std::string fpath;
	CSVFileManager *fmanager;
	ecs::ECS_Coordinator* ECS_manager;
	std::vector<std::vector<ecs::entity*>> map_array;

	//initializes all entities and components for our map
	virtual bool InitializeMap(std::vector<std::vector<std::string>> cell_info, std::vector<std::vector<ecs::entityId>> cell_file_ids) = 0;
};

#endif // !MAP_H