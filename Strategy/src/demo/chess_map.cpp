#include "chess_map.h"

bool ChessMap::InitializeMap(std::vector<std::vector<std::string>> cell_info, std::vector<std::vector<ecs::entityId>> file_ids) {
	PieceType* piece_type;
	for (int j = 0; j < file_ids.size(); j++) {
		this->map_array.resize(file_ids.size());
		for (int i = 0; i < file_ids[j].size(); i++) {
			this->map_array[j].push_back(this->ECS_manager->CreateEntity(cell_info[j][1]));
			ecs::ComponentManager* comp_manager = ecs::ComponentManager::GetComponentManager();
			piece_type = new PieceType(stoi(cell_info[j * (file_ids[j].size()) + i][2]));
			comp_manager->AddComponent<PieceType>(this->map_array[j][i], piece_type->type_name, piece_type);
		}
	}

	return true;
}

void ChessMap::Tick() {
}

void ChessMap::Render(SDL_Renderer*& r) {
	for (int j = 0; j < this->map_array.size(); j++) {
		for (int i = 0; i < this->map_array[j].size(); i++) {
			uint8_t piece_type = this->ECS_manager->GetComponent<PieceType>(this->map_array[j][i], "piece_type_component")->type;
			SDL_Rect fillRect = { i * 30, j * 30, 30 , 30 };
			switch (piece_type) {
			case(0):
				SDL_SetRenderDrawColor(r, 0xff, 0xff, 0xff, 0xff);
				break;
			case(1):
				SDL_SetRenderDrawColor(r, 0x00, 0x00, 0x00, 0xff);
				break;
			default:
				SDL_SetRenderDrawColor(r, 0xFF, 0x00, 0x00, 0xFF);
				break;
			}SDL_RenderFillRect(r, &fillRect);
		}
	}
}
