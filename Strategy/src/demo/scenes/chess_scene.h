#ifndef CHESS_SCENE_H
#define CHESS_SCENE_H

#include "../../scenes/iscene.h"
#include "../chess_map.h"

class ChessScene: public IScene {
public:
	ChessScene(IGame& game, SDL_Renderer*& r, Keyboard*& key, std::string& path) : IScene(game, r, key, path) , map(path){}
	void Tick();
	void Render();
private:
	ChessMap map;
};

#endif // !CHESS_SCENE_H