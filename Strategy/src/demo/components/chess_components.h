#ifndef CHESS_COMPONENTS_H
#define CHESS_COMPONENTS_H

#include <cstdint>

#include "../../ecs/component/component.h"

struct PieceType : ecs::Component {
	PieceType(uint8_t type) : ecs::Component("piece_type_component"), type(type) {}
	uint8_t type;
};

#endif // !CHESS_COMPONENTS_H

