#include "test_map.h"

bool TestMap::InitializeMap(std::vector<std::vector<std::string>> cell_info, std::vector<std::vector<ecs::entityId>> file_ids) {
	ecs::defaultComponents::CellType* cell_type;
	for (int j = 0; j < file_ids.size(); j++) {
		this->map_array.resize(file_ids.size());
		for (int i = 0; i < file_ids[j].size(); i++) {
			this->map_array[j].push_back(this->ECS_manager->CreateEntity("cell_entity"));
			ecs::ComponentManager* comp_manager = ecs::ComponentManager::GetComponentManager();
			cell_type = new ecs::defaultComponents::CellType(cell_info[j * (file_ids[j].size()) + i][1]);
			comp_manager->AddComponent<ecs::defaultComponents::CellType>(this->map_array[j][i], cell_type->type_name, cell_type);
		}
	}

	return true;
}

void TestMap::Tick(){
}

void TestMap::Render(SDL_Renderer*& r) {
	for (int j = 0; j < this->map_array.size(); j++) {
		for (int i = 0; i < this->map_array[j].size(); i++) {
			uint8_t cell_type = this->ECS_manager->GetComponent<ecs::defaultComponents::CellType>(this->map_array[j][i], "cell_type_component")->type;
			SDL_Rect fillRect = { i * 30, j * 30, 30 , 30 };
			switch (cell_type) {
			case((uint8_t)ecs::defaultComponents::CellType::cellType::WATER):
				SDL_SetRenderDrawColor(r, 0x00, 0x00, 0xFF, 0xFF);
				break;
			case((uint8_t)ecs::defaultComponents::CellType::cellType::PLAINS):
				SDL_SetRenderDrawColor(r, 0x90, 0xEE, 0x90, 0xFF);
				break;
			case((uint8_t)ecs::defaultComponents::CellType::cellType::FOREST):
				SDL_SetRenderDrawColor(r, 0x00, 0x80, 0x00, 0xFF);
				break;
			case((uint8_t)ecs::defaultComponents::CellType::cellType::HILLS):
				SDL_SetRenderDrawColor(r, 0x80, 0x80, 0x00, 0xFF);
				break;
			default:
				SDL_SetRenderDrawColor(r, 0xFF, 0x00, 0x00, 0xFF);
				break;
			}SDL_RenderFillRect(r, &fillRect);
		}
	}
}
