#include "game.h"

int main(int argc, char* argv[]) {

	Game* game = new Game();

	int exitCode = game->Run();

	delete game;
	return exitCode;
}
