#ifndef GAME_STATE_H

#include <string>

#include "../../states/istate.h"
#include "../scenes/chess_scene.h"

class GameState:public IState{
public:
	GameState(IGame& game, SDL_Renderer*& r, Keyboard*& key, std::string& path) : IState(game, r, key, path), scene(game, r, key, path) {
		//init scene with everything
	}

	void Tick();
	void Render();
private:
	ChessScene scene;
};

#endif // GAME_STATE_H