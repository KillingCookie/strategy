#ifndef CHESS_MAP_H
#define CHESS_MAP_H

#endif // !CHESS_MAP_H

#include "../map/imap.h"
#include "components/chess_components.h"
class ChessMap : public IMap {
public:
	ChessMap(std::string fpath) : IMap(fpath) {
		this->fmanager = new CSVFileManager(fpath);
		this->ECS_manager = ecs::ECS_Coordinator::GetECS_Coordinator();

		std::vector<std::vector<ecs::entityId>> cell_file_ids = this->fmanager->ReadFile<uint16_t>();
		delete this->fmanager;
		fpath.replace(fpath.length() - 7, std::string::npos, "cell.svf");//7 for Map.csv
		this->fmanager = new CSVFileManager(fpath);
		InitializeMap(fmanager->ReadFile<std::string>(), cell_file_ids);
	}

	virtual void Tick();
	virtual void Render(SDL_Renderer*& r);
private:
	//creates an entity array of the map - map_array, and initializes components for each tile
	bool InitializeMap(std::vector<std::vector<std::string>> cell_info, std::vector<std::vector<ecs::entityId>> cell_file_ids) override;
};

