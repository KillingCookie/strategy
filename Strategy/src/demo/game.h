#ifndef GAME_H
#define GAME_H

#include "../igame.h"
#include "../states/state_manager.h"
#include "states/game_state.h"

class Game : public IGame {
public:
	Game() :IGame() {
		this->state_manager->SetState<GameState>(*this, r, key, "csv/chess_map.svf");
	}
private:
};

#endif // !GAME_H

