#ifndef SCENE_H
#define SCENE_H

#include "SDL.h"

#include "../igame.h"
#include "../input/Keyboard.h"
#include "../map/imap.h"

class IScene {
public:
	IScene(IGame& game, SDL_Renderer*& r, Keyboard*& key, std::string& path): game(game), r(r), key(key), folder_path(path){}
	virtual void Tick() = 0;
	virtual void Render() = 0;
protected:
	IGame& game;
	SDL_Renderer*& r;
	Keyboard*& key;
	std::string& folder_path;
};

#endif // !SCENE_H

