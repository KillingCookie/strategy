#ifndef TIME_MANAGER_H
#define TIME_MANAGER_H

#include <cstdint>
#include <string>

/*Singleton, should be initialized throug static GetTimeManager*/
class TimeManager {
public:
	~TimeManager() {
		delete instance;
	}

	enum class timeType { NOT_SET = -1, TURN_BASED};

	//init clock with a flag from enum class timeType::
	void StartClock(timeType type) {
		if(this->time_type == timeType::NOT_SET) this->time_type = type;
	}

	/*(1, "Turn") means 1 unit of time = 1 Turn
	* (5.5, "Year") means 1 unit of time = 5.5 years*/
	void SetTimeFrame(double modifier, std::string unit_name) {
		//timeframe might be a non-linear function as in the civilization games
		//add an overloaded function which takes a functions pointer(can i give a lambda as a parameter
		this->time_modifier = modifier;
		this->unit_name = unit_name;
	}

	//increases time by 1
	void Tick() {
		this->current_time++;
	}

	//get a string with current time
	std::string getTime() {
		char time[50] = "";
		if (this->time_type == timeType::TURN_BASED) {
			sprintf(time, "Turn %.2f %s", static_cast<double>(this->current_time)* (this->time_modifier), unit_name);
		}
		return time;
	}

	static TimeManager* GetTimeManager() {
		if (instance == nullptr) instance = new TimeManager();
		return instance;
	}
	
private:
	TimeManager() {}
	static TimeManager* instance;
	uint32_t current_time = 0;
	timeType time_type = timeType::NOT_SET;
	double time_modifier = 1.0f;
	std::string unit_name;
};

#endif // !TIME_MANAGER_H

