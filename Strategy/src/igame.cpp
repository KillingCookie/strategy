#include "igame.h"
#include "states\state_manager.h"
#include "input\keyboard.h"

IGame::IGame() {
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
		//std::cout << "sdl init successfull...\n";

	if (win = SDL_CreateWindow("Str", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 480, 360, SDL_WINDOW_SHOWN))
		//std::cout << "window created...\n";

	if (r = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC))
		//std::cout << "renderer created...\n";

	is_running = true;
	//std::cout << "game running..." << std::endl;

	//fps = 60;

	key = new Keyboard(this);
	state_manager = StateManager::GetInstance();
	//state_manager->SetState<GameState>(*this, r, key, "csv/testMap.csv");
}

IGame::~IGame() {
	delete key;
	delete state_manager;

	SDL_DestroyWindow(win);
	SDL_DestroyRenderer(r);
	SDL_Quit();
}

void IGame::Tick() {
	state_manager->TickState();
}

void IGame::Render() {
	SDL_SetRenderDrawColor(r, 0, 0, 0, 0);//defauld color of render area is black
	SDL_RenderClear(r);

	state_manager->RenderState();

	SDL_RenderPresent(r);
}

void IGame::EventLoop() {

	/*while (SDL_PollEvent(&e) != 0){

		if (e.type == SDL_QUIT)
			isRunning = true;

	}*/

	key->tick();

	SDL_PollEvent(&e);//poll

	switch (e.type) {
	case SDL_QUIT:
		is_running = false;
		break;
	default:
		break;
	}
}

// Main loop of the game, which calls render, tick and polls for events
int IGame::Run() {
	/* I should add a capability of changing the FPS*/
	const int FPS = 60;
	const int frameDelay = 1000 / FPS;

	Uint32 frame_start;
	int frame_time;

	while (is_running) {
		frame_start = SDL_GetTicks();

		this->EventLoop();
		this->Tick();
		this->Render();

		frame_time = SDL_GetTicks() - frame_start;
		if (frame_time < frameDelay) SDL_Delay(frameDelay - frame_time);
	}

	return 0;

}

/*void Game::setFPS(int fps) {

	this->fps = fps;

}*/
