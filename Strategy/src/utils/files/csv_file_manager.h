#ifndef CSV_FILE_MANAGER_H
#define CSV_FILE_MANAGER_H

/*
* The CSV's used by this engine should start from header which consists of the size(height, width) of the array of all the file contents.
* This header will be read inside constructor by a private ReadHeader() function, which defines Vecor2D size variable.
* Every CSV file within this project must be named .svf
*/

#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdexcept>

#include "../../physics/vector2d.h"

/*no standart constructor
* there should be one instance for each file
*/
class CSVFileManager {
public:
	//create function read header and init all obj variables from there
	CSVFileManager(std::string& path) : path(path) {
		if (!ReadHeader()) throw std::invalid_argument("The size of file contents is not set " + path);//try catch
	}
	//~CSVFileManager();

	int WriteToFile(std::string& path);

	//bool isCorrupted();

	template<typename T>
	std::vector<std::vector<T>> ReadFile(){
		int i, j;
		T value;
		std::vector<std::vector<T>> vec;
		std::string line;// value = "";
		std::ifstream file;
		std::stringstream sline;

		file.open(this->path);

		i = 0; j = 0;
		vec.resize(this->size.y);
		for (int e = 0; e < this->size.y; e++) vec[e].reserve(this->size.x);
		std::getline(file, line, '\n');//geth through header

		while (std::getline(file, line, ';')) {//all spaces in-between separator must be filled
			sline.str(line);
			while (sline >> value) {
				//if (sline.peek() == this->separator) {
					vec[j].push_back(value);
					//sline.ignore();
				//}
			}
			sline.clear();
			i++;
			if (i % static_cast<int>(this->size.x) == 0) {
				j++; 
				i = 0;
			}
		}
		file.close();
		return vec;
	}

private:
	CSVFileManager() = delete;
	Vector2D size;
	char separator = ';';
	std::string& path;
	//bool isCorrupted;

	bool ReadHeader();
};

#endif