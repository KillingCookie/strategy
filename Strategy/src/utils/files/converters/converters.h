#ifndef CONVERTERS_H
#define CONVERTERS_H

#include <cstdint>
#include <unordered_map>
#include <vector>

namespace converters {

	// makes unordered map from 2d vector with a key of a given position and type
	template<typename T, typename K>
	std::unordered_map<T, std::vector<K>> Convert2DVectorIntoUnorderedMap(std::vector<std::vector<K>> vector, uint8_t index) {
		std::unordered_map<T, std::vector<K>> map;
		for (int j = 0; j < vector.size(); j++) {
			map.insert({ vector[j][index], vector[j] })
		}
		return map;
	}

}

#endif // !CONVERTERS_H

