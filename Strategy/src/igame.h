#ifndef IGAME_H
#define IGAME_H

#include "SDL.h"

//#include "states\State.h"
//#include "states\GameState.h"
//#include "input\Keyboard.h"

//class State;
class StateManager;
class GameState;
class Keyboard;

/*you should define state_manager inside derived-class constructor*/
class IGame {
public:
	IGame();
	~IGame();

	void Tick();
	void Render();
	void EventLoop();
	int Run();

	//void setFPS(int fps);

	SDL_Event* GetEvent() { return &this->e; }

protected:
	//int fps;

	SDL_Window* win;
	SDL_Renderer* r;
	SDL_Event e;
	Keyboard* key;

	StateManager* state_manager;

	bool is_running = false;
};

#endif IGAME_H